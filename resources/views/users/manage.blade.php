@extends('layouts.template')

@section('content')
<!-- Small boxes (Stat box) -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb" style="left:0;padding-left: 20px;">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('/users') }}"><i class="fa fa-users"></i>Manage Users</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Users</h3>
                  
                  <div class="btn btn-primary pull-right" id="add_btn"><i class="fa fa-plus"></i> Add</div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="users-table" class="table table-bordered table-hover table-responsive">
                    <thead>
                      <tr>
                        <th class="col-md-1 text-center">ID</th>
                        <th class="col-md-4">Fullname</th>
                        <th class="col-md-3">Email</th>
                        <th class="col-md-1">Role</th>
                        <th class="col-md-3">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="col-md-1 text-center">1</td>
                        <td>Joel Baluma</td>
                        <td>baluma.joel91@gmail.com</td>
                        <td>System Admin</td>
                        <td>
                          <div class="btn btn-primary btn-xs perm-btn"><i class="fa fa-eye"></i> Permissions</div>
                          <div class="btn btn-info btn-xs edit-btn"><i class="fa fa-edit"></i> Edit</div>
                          <div class="btn btn-danger btn-xs del-btn"><i class="fa fa-trash"></i> Delete</div>
                        </td>
                      </tr>

                      </tbody>
                    <tfoot>
                      <tr>
                        <th class="col-md-1 text-center">ID</th>
                        <th class="col-md-4">Fullname</th>
                        <th class="col-md-3">Email</th>
                        <th class="col-md-1">Role</th>
                        <th class="col-md-3">Actions</th>

                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- DOCK MODALS -->
       
        <div id="add-user-dialog" title="Add User" style="display: none;">
          <form id="add-user-form"  method="post" role="form"  action="/admin/users">
            {{ csrf_field() }}  
              <div class="form-group">
                <label>Fullname:</label>
                <input type="text" class="form-control" placeholder="Enter Fullname" name="name">
              </div> 

              <div class="form-group">
                <label>Username:</label>
                <input type="text" class="form-control" placeholder="Enter Username" name="username">
              </div> 

              <div class="form-group">
                <label>Email Address: </label>
                <input type="email" class="form-control" placeholder="Email Address" name="email">
              </div>
              
              
              <div class="form-group">
                <label>Role:</label>
                <select class="form-control select2" style="width: 100%;" name="type">
                  <option value="Admin">Admin</option>
                  <option value="Manager">Manager</option>
                  <option value="User" selected>User</option>
                </select>
              </div><!-- /.form-group -->
              
              
              <div class="form-group">
                <label>Password:</label>
                <input type="password" class="form-control" placeholder="Password" name="password">
              </div>             
              <div class="form-group">
                <label>Confirm Password:</label>
                <input type="password" class="form-control" placeholder="Confirm Password" name="password_2nd">
              </div>             
          </form>
        </div>

        <div id="edit-user-dialog" title="Edit User" style="display: none;">
        </div>
        
        <div id="view-user-dialog" title="View Full Details User">
        </div>
      <!-- DOCK MODALS -->
@endsection

@section('css')  
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dockmodal/jquery.dockmodal.css') }}">
    <link rel="stylesheet" href="{{ asset('dockmodal/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('dockmodal/prettify.css') }}">

    <style type="text/css">
      .select2-container--default .select2-selection--single{
            height: 35px;
      }
      .dockmodal-body{
        overflow-x: hidden;
      }
    </style>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('dockmodal/prettify.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('dockmodal/prettify_google.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('dockmodal/jquery.dockmodal.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('dockmodal/sweetalert-dev.js') }}"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\AddUserRequest', '#add-user-form') !!}
   @include('users.script')
@endsection