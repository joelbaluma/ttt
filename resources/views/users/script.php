    
<script type="text/javascript">
/*	$(function(){
		$('#users-table').DataTable();

	});
*/	$(document).ready(function(){
        uTable = $("#users-table").dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": '/admin/get-all-users',
            "lengthMenu" : [
                [25, 100, -1],
                [25, 100, "All"]
            ],
            "columns": [
            	{data: 'id', name: 'id', className: 'col-md-1 text-center'},
                {data: 'name', name: 'name', className: 'col-md-3'},
                {data: 'email', name: 'email', className: 'col-md-3'},
                {data: 'type', name: 'type', className: 'col-md-2'},
                {data: 'action', name: 'id', className: 'col-md-3 text-center'},
            ]
        });
    });

     $("#add_btn").click(function () {
        $("#add-user-dialog").dockmodal({
            title:"<i class='fa fa-users'> </i> Add User",
            initialState:"docked",
            width: 400,
            height: 550,
            animationSpeed: 200,
            buttons:[
                {
                    html:'<i class="fa fa-times"></i> Cancel',
                    buttonClass:'btn btn-danger pull-left',
                    click:function (e, dialog) {
                        dialog.dockmodal("close");
                    }
                },
                {
                    html:'<i class="fa fa-plus"></i> Add',
                    buttonClass:'btn btn-success',
                    click:function (e, dialog) {
                        
                        /** AJAX ADD STARTED **/
                         $.ajax({
                                    type: 'POST',
                                    url: '/admin/users',
                                    data: $("#add-user-form").serialize(),
                                    dataType: 'json',
                                    success: function(data){
                                    
                                    }
                                }
                        ).done(function(data) {
                                if(data.success){
                                    swal({
                                        title: "User added successfully!",
                                        text: data.msg,
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#4CAF50",
                                        confirmButtonText: "Okay",
                                    },
                                    function(isConfirm){
                                       // window.location.reload();
                                       $("#users-table").dataTable().fnDraw();
                                       $('#add-user-form')[0].reset();
                                       $("[name='type']").val(0).change();
                                    });             
                                }else{
                                    swal("Error!", data.msg, "error");
                                }

                                $("#users-table").dataTable().fnDraw();
                            })
                        .error(function(data) {
                            swal("Oops", "Something went wrong!", "error");
                        });
                        /** AJAX ADD ENDED **/
                        dialog.dockmodal("close");         
                    }
                }
            ],
            open:function(){
                $("#add-user-dialog").show();
            },
            close: function(){
                $("#add-user-dialog").hide();
            }
        });
        return false;
    });




    $(document).on('click', '.perm-btn', function(){

        $("#view-user-dialog").dockmodal({
            title:"<i class='fa fa-users'> </i> User Permissions",
            initialState:"modal",
            width: 400,
            height: 600,
            animationSpeed: 400,
            poppedOutDistance: "16%",
            buttons:[
                {
                    html:'<i class="fa fa-times"></i> Cancel',
                    buttonClass:'btn btn-danger pull-left',
                    click:function (e, dialog) {
                        dialog.dockmodal("close");
                    }
                },
                {
                    html:'<i class="fa fa-plus"></i> Add',
                    buttonClass:'btn btn-success',
                    click:function (e, dialog) {

                        ajax_add("Add User", $("#add-user-form"), 'admin/user');
                        dialog.dockmodal("close");         
                    }
                }
            ],
            open:function(){
                if(!$('body.sidebar-mini').hasClass('sidebar-collapse')){
                   // $(".sidebar-toggle").click()
                }
                $("#view-user-dialog").show();
            },
            close: function(){
                $("#view-user-dialog").hide();
                if($('body.sidebar-mini').hasClass('sidebar-collapse')){
                   // $(".sidebar-toggle").click()
                }
            }
        });
        return false;
    });


    $(document).on('click', ".edit-btn", function(){
        $("#edit-user-dialog").removeClass('hidden');
        $("#edit-user-dialog").dockmodal({
            title:"<i class='fa fa-edit'> </i> Edit User",
            initialState:"docked",
            width: 400,
            height: 550,
            buttons:[
                {
                    html:'<i class="fa fa-times"></i> Cancel',
                    buttonClass:'btn btn-danger pull-left',
                    click:function (e, dialog) {
                        dialog.dockmodal("close");
                        $("#edit-user-dialog").hide();
                    }
                },
                {
                    html:'<i class="fa fa-save"></i> Save',
                    buttonClass:'btn btn-primary',
                    click:function (e, dialog) {


                        dialog.dockmodal("close");
                        $("#edit-user-dialog").hide();
                    }
                }
            ],
            close: function(){
                $("#edit-user-dialog").hide();
            }
        });
        return false;
    });

    $(document).on('click', '.del-btn', function(){

        swal({
          title: "Delete User?",
          text: "Are you sure to delete user?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel it!",
          cancelButtonColor: "#1c910e",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
                swal({
                  title: "Delete User!",
                  text: "Enter password to delete user:",
                  type: "input",
                  inputType: "password",
                  showCancelButton: true,
                  closeOnConfirm: false,
                  animation: "slide-from-top",
                  inputPlaceholder: "Enter Password"
                },
                function(inputValue){


                  if (inputValue === null) {
                    return false;
                  }
                  
                  if (inputValue === "") {
                    swal.showInputError("You need to write something!");
                    return false
                  }
                  
                  swal("Success!", "User successfully deleted!", "success");
                });
          } else {
            swal("Cancelled", "The user is safe :)", "error");
          }
        });
    });


     $(function(){
        $(".select2").select2();
     });

     function ajax_add(msg, data, link){
        $.ajax(
                {
                    type: 'POST',
                    url: link,
                    data: data,
                    dataType: 'json',
                    success: function(data){
                    
                    }
                }
        ).done(function(data) {
                if(data.success){
                    swal({
                        title: msg+" Success!",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#4CAF50",
                        confirmButtonText: "Okay",
                    },
                    function(isConfirm){
                        window.location.reload();
                    });             
                }else{
                    swal("Error!", data.msg, "error");
                }

                table.dataTable().fnDraw();
            })
        .error(function(data) {
            swal("Oops", "We couldn't connect to the server!", "error");
        });
     }
</script>