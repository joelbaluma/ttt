<!DOCTYPE html>
<html lang="en">
	<head>
	@include('layouts.top_header')
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		@include('layouts.header')
		@include('layouts.sidebar')
		
			@yield('content')


		@include('layouts.footer')
		@yield('scripts')
	</body>
</html>
