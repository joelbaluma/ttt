@extends('auth.app')

@section('main_content')
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>Dashboard</a>
  </div><!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    @if(Request::path()=='logout')
      <div class="alert alert-success alert">
        <h4>  <i class="icon fa fa-check"></i> Successfully Logout!</h4>
        <form action="{{url('login')}}" method="GET">
        <button class="btn btn-block btn-flat btn-success" href="{{url('/login')}}"><i class="fa fa-login"></i>Click here to login...</button>
        </form>
      </div>
    @else
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }} ">
        <input type="email" name="email"  id="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div><!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div><!-- /.col -->
      </div>
    </form>
    @endif
    <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
    <a href="{{ url('register') }}" class="text-center">Register a new membership</a>

  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
@endsection
