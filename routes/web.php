<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();

/***** AUTH METHODS ******/

Route::get('/', function () {
    return redirect('/admin');
})->middleware('auth');
Route::get('/home', function () {
    return redirect('/admin');
})->middleware('auth');

Route::get('logout', function(){
	Auth::logout();
	return view('auth/login');
});


/*************************/

Route::group(['middleware' => 'auth','prefix' => 'admin'], function () {

	Route::resource('/', 'HomeController@index');
	Route::resource('users', 'UserController');
	Route::get('get-all-users', 'UserController@get_all_users');
});

