<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5',
            'password_2nd' => 'required|same:password|min:5',
        ];
    }

    public function messages()
    {
        return [
            'password_2nd.same' => 'Password does not match!',
            'password_2nd.min' => 'Password does not match!',
        ];
    }
}
