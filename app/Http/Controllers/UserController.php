<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Datatables;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users/manage');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new \App\Users;
        $user->name = $request->get('name');
        $user->username = $request->get('username');
        $user->image_link = null;
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get("password"));
        $user->type =  $request->get('type');
        if($user->save()){
            return response()->json(['success' => true, 'msg' => 'User successfully Added']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while adding new user!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_user = Auth::user()->find($id);
        $deletedRows = Auth::user()->where('id', $id)->delete();
        if($deletedRows){
            return response()->json(['success' => true, 'msg' => 'User successfully Deleted!']);
        }else{
            return response()->json(['success' => false, 'msg' => 'An error occured while deleting user!']);
        }
        
    }

    public function get_all_users(){

        $users= Auth::user()->get();
        
        return Datatables::of($users)
            
             
            ->AddColumn('id', function($column){
               return $column->id;
            })
            ->AddColumn('name', function($column){
               return $column->name;
            })
            ->AddColumn('email', function($column){
               return $column->email;
            })
            ->AddColumn('type', function($column){
               return $column->type;
            })
            ->AddColumn('action', function($column){
              // return '<a href="#" class="btn btn-info btn-xs" data-id="'.$column->id.'"><i class="fa fa-edit"></i> Edit</a>
              //            <a href="#" class="btn btn-danger btn-xs" data-id="'.$column->id.'"><i class="fa fa-trash"></i> Delete</a>';
                return '<div class="btn btn-primary btn-xs perm-btn"  data-id="'.$column->id.'"><i class="fa fa-eye"></i> Details</div>
                          <div class="btn btn-info btn-xs edit-btn"  data-id="'.$column->id.'"><i class="fa fa-edit"></i> Edit</div>
                          <div class="btn btn-danger btn-xs del-btn"  data-id="'.$column->id.'"><i class="fa fa-trash"></i> Delete</div>';
            })
            

            ->make(true);   

    }
}
